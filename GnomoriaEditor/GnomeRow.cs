using System.ComponentModel;
using Game;
using GameLibrary;

namespace GnomoriaEditor
{
    public class GnomeRow : CharacterRow, IEditableObject
    {
        public Profession Profession { get; set; }

        public int Mining { get; set; }
        public int Masonry { get; set; }
        public int Stonecarving { get; set; }
        public int WoodCutting { get; set; }
        public int Carpentry { get; set; }
        public int Woodcarving { get; set; }
        public int Smelting { get; set; }
        public int Blacksmithing { get; set; }
        public int Metalworking { get; set; }
        public int WeaponCrafting { get; set; }
        public int ArmorCrafting { get; set; }
        public int Gemcutting { get; set; }
        public int JewelryMaking { get; set; }
        public int Weaving { get; set; }
        public int Tailoring { get; set; }
        public int Pottery { get; set; }
        public int Leatherworking { get; set; }
        public int Bonecarving { get; set; }
        public int Prospecting { get; set; }
        public int Tinkering { get; set; }
        public int Machining { get; set; }
        public int Engineering { get; set; }
        public int Mechanic { get; set; }
        public int AnimalHusbandry { get; set; }
        public int Butchery { get; set; }
        public int Horticulture { get; set; }
        public int Farming { get; set; }
        public int Cooking { get; set; }
        public int Brewing { get; set; }
        public int Medic { get; set; }
        public int Caretaking { get; set; }
        public int Construction { get; set; }
        public int Hauling { get; set; }

        public GnomeRow(Character character) : base(character)
        {
            Name = character.Name();
            Profession = character.Mind.Profession;

            Mining = character.SkillLevel(CharacterSkillType.Mining);
            Masonry = character.SkillLevel(CharacterSkillType.Masonry);
            Stonecarving = character.SkillLevel(CharacterSkillType.Stonecarving);
            WoodCutting = character.SkillLevel(CharacterSkillType.WoodCutting);
            Carpentry = character.SkillLevel(CharacterSkillType.Carpentry);
            Woodcarving = character.SkillLevel(CharacterSkillType.Woodcarving);
            Smelting = character.SkillLevel(CharacterSkillType.Smelting);
            Blacksmithing = character.SkillLevel(CharacterSkillType.Blacksmithing);
            Metalworking = character.SkillLevel(CharacterSkillType.Metalworking);
            WeaponCrafting = character.SkillLevel(CharacterSkillType.WeaponCrafting);
            ArmorCrafting = character.SkillLevel(CharacterSkillType.ArmorCrafting);
            Gemcutting = character.SkillLevel(CharacterSkillType.GemCutting);
            JewelryMaking = character.SkillLevel(CharacterSkillType.JewelryMaking);
            Weaving = character.SkillLevel(CharacterSkillType.Weaving);
            Tailoring = character.SkillLevel(CharacterSkillType.Tailoring);
            Pottery = character.SkillLevel(CharacterSkillType.Pottery);
            Leatherworking = character.SkillLevel(CharacterSkillType.Leatherworking);
            Bonecarving = character.SkillLevel(CharacterSkillType.Bonecarving);
            Prospecting = character.SkillLevel(CharacterSkillType.Prospecting);
            Tinkering = character.SkillLevel(CharacterSkillType.Tinkering);
            Machining = character.SkillLevel(CharacterSkillType.Machining);
            Engineering = character.SkillLevel(CharacterSkillType.Engineering);
            Mechanic = character.SkillLevel(CharacterSkillType.Mechanic);
            AnimalHusbandry = character.SkillLevel(CharacterSkillType.AnimalHusbandry);
            Butchery = character.SkillLevel(CharacterSkillType.Butchery);
            Horticulture = character.SkillLevel(CharacterSkillType.Horticulture);
            Farming = character.SkillLevel(CharacterSkillType.Farming);
            Cooking = character.SkillLevel(CharacterSkillType.Cooking);
            Brewing = character.SkillLevel(CharacterSkillType.Brewing);
            Medic = character.SkillLevel(CharacterSkillType.Medic);
            Caretaking = character.SkillLevel(CharacterSkillType.Caretaker);
            Construction = character.SkillLevel(CharacterSkillType.Construction);
            Hauling = character.SkillLevel(CharacterSkillType.Hauling);
        }

        public override void Save()
        {
            base.Save();

            Character.SetName(Name);
            Character.Mind.Profession = Profession;

            Character.SetSkillLevel(CharacterSkillType.Mining, Mining);
            Character.SetSkillLevel(CharacterSkillType.Masonry, Masonry);
            Character.SetSkillLevel(CharacterSkillType.Stonecarving, Stonecarving);
            Character.SetSkillLevel(CharacterSkillType.WoodCutting, WoodCutting);
            Character.SetSkillLevel(CharacterSkillType.Carpentry, Carpentry);
            Character.SetSkillLevel(CharacterSkillType.Woodcarving, Woodcarving);
            Character.SetSkillLevel(CharacterSkillType.Smelting, Smelting);
            Character.SetSkillLevel(CharacterSkillType.Blacksmithing, Blacksmithing);
            Character.SetSkillLevel(CharacterSkillType.Metalworking, Metalworking);
            Character.SetSkillLevel(CharacterSkillType.WeaponCrafting, WeaponCrafting);
            Character.SetSkillLevel(CharacterSkillType.ArmorCrafting, ArmorCrafting);
            Character.SetSkillLevel(CharacterSkillType.GemCutting, Gemcutting);
            Character.SetSkillLevel(CharacterSkillType.JewelryMaking, JewelryMaking);
            Character.SetSkillLevel(CharacterSkillType.Weaving, Weaving);
            Character.SetSkillLevel(CharacterSkillType.Tailoring, Tailoring);
            Character.SetSkillLevel(CharacterSkillType.Pottery, Pottery);
            Character.SetSkillLevel(CharacterSkillType.Leatherworking, Leatherworking);
            Character.SetSkillLevel(CharacterSkillType.Bonecarving, Bonecarving);
            Character.SetSkillLevel(CharacterSkillType.Prospecting, Prospecting);
            Character.SetSkillLevel(CharacterSkillType.Tinkering, Tinkering);
            Character.SetSkillLevel(CharacterSkillType.Machining, Machining);
            Character.SetSkillLevel(CharacterSkillType.Engineering, Engineering);
            Character.SetSkillLevel(CharacterSkillType.Mechanic, Mechanic);
            Character.SetSkillLevel(CharacterSkillType.AnimalHusbandry, AnimalHusbandry);
            Character.SetSkillLevel(CharacterSkillType.Butchery, Butchery);
            Character.SetSkillLevel(CharacterSkillType.Horticulture, Horticulture);
            Character.SetSkillLevel(CharacterSkillType.Farming, Farming);
            Character.SetSkillLevel(CharacterSkillType.Cooking, Cooking);
            Character.SetSkillLevel(CharacterSkillType.Brewing, Brewing);
            Character.SetSkillLevel(CharacterSkillType.Medic, Medic);
            Character.SetSkillLevel(CharacterSkillType.Caretaker, Caretaking);
            Character.SetSkillLevel(CharacterSkillType.Construction, Construction);
            Character.SetSkillLevel(CharacterSkillType.Hauling, Hauling);
        }

        public void SetProfessionSkills(int skillValue)
        {
            Mining = skillValue;
            Masonry = skillValue;
            Stonecarving = skillValue;
            WoodCutting = skillValue;
            Carpentry = skillValue;
            Woodcarving = skillValue;
            Smelting = skillValue;
            Blacksmithing = skillValue;
            Metalworking = skillValue;
            WeaponCrafting = skillValue;
            ArmorCrafting = skillValue;
            Gemcutting = skillValue;
            JewelryMaking = skillValue;
            Weaving = skillValue;
            Tailoring = skillValue;
            Pottery = skillValue;
            Leatherworking = skillValue;
            Bonecarving = skillValue;
            Prospecting = skillValue;
            Tinkering = skillValue;
            Machining = skillValue;
            Engineering = skillValue;
            Mechanic = skillValue;
            AnimalHusbandry = skillValue;
            Butchery = skillValue;
            Horticulture = skillValue;
            Farming = skillValue;
            Cooking = skillValue;
            Brewing = skillValue;
            Medic = skillValue;
            Caretaking = skillValue;
            Construction = skillValue;
            Hauling = skillValue;
        }

        public void BeginEdit()
        {
        }

        public void EndEdit()
        {
            Save();
        }

        public void CancelEdit()
        {
        }
    }
}