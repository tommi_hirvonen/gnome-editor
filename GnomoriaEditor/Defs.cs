﻿using System.Linq;
using Game;
using GameLibrary;

namespace GnomoriaEditor
{
    public class Defs
    {
        public static FactionDef NeutralFactionDef
        {
            get { return GnomanEmpire.Instance.World.AIDirector.FactionDefs.First(x => x.Type == FactionType.Neutral); }
        }

        public static FactionDef EnemyFactionDef
        {
            get { return GnomanEmpire.Instance.World.AIDirector.FactionDefs.First(x => x.Type == FactionType.EnemyCiv); }
        }

        public static FactionDef WildFactionDef
        {
            get { return GnomanEmpire.Instance.World.AIDirector.FactionDefs.First(x => x.Type == FactionType.Wild); }
        }

        public static FactionDef PlayerFactionDef
        {
            get { return GnomanEmpire.Instance.World.AIDirector.FactionDefs.First(x => x.Type == FactionType.PlayerCiv); }
        }

        public static RaceClassDef YakDef
        {
            get { return NeutralFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.Yak)).Classes.First(x => x.Race == RaceID.Yak); }
        }

        public static RaceClassDef AlpacaDef
        {
            get { return NeutralFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.Alpaca)).Classes.First(x => x.Race == RaceID.Alpaca); }
        }

        public static RaceClassDef BearDef
        {
            get { return WildFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.Bear)).Classes.First(x => x.Race == RaceID.Bear); }
        }

        public static RaceClassDef EmuDef
        {
            get { return NeutralFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.Emu)).Classes.First(x => x.Race == RaceID.Emu); }
        }

        public static RaceClassDef GnomeDef
        {
            get { return PlayerFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.Gnome)).Classes.First(x => x.Race == RaceID.Gnome); }
        }

        public static RaceClassDef HoneyBadgerDef
        {
            get { return WildFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.HoneyBadger)).Classes.First(x => x.Race == RaceID.HoneyBadger ); }
        }

        public static RaceClassDef MonitorLizardDef
        {
            get { return WildFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.MonitorLizard)).Classes.First(x => x.Race == RaceID.MonitorLizard); }
        }

        public static RaceClassDef OgreDef
        {
            get { return EnemyFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.Ogre)).Classes.First(x => x.Race == RaceID.Ogre); }
        }

        public static RaceClassDef ToughOgreDef
        {
            get { return EnemyFactionDef.Squads.First(x => x.Classes.Any(y => y.Race == RaceID.BlueOgre)).Classes.First(x => x.Race == RaceID.BlueOgre); }
        }

        public static Faction WildFaction
        {
            get { return GnomanEmpire.Instance.World.AIDirector.Factions.First(x => x.Value.FactionDef.Type == FactionType.Wild).Value; }
        }

        public static Faction NeutralFaction
        {
            get { return GnomanEmpire.Instance.World.AIDirector.NeutralFaction; }
        }

        public static Faction EnemyFaction
        {
            get { return GnomanEmpire.Instance.World.AIDirector.Factions.First(x => x.Value.FactionDef.Type == FactionType.EnemyCiv).Value; }
        }

        public static Faction PlayerFaction
        {
            get { return GnomanEmpire.Instance.World.AIDirector.PlayerFaction; }
        }

    }
}