using Game;
using GameLibrary;

namespace GnomoriaEditor
{
    public abstract class CharacterRow
    {
        protected Character Character;

        public uint Id { get; set; }
        public string Name { get; set; }
        public int Fitness { get; set; }
        public int Nimbleness { get; set; }
        public int Curiosity { get; set; }
        public int Focus { get; set; }
        public int Charm { get; set; }
        public int Fighting { get; set; }
        public int Brawling { get; set; }
        public int Sword { get; set; }
        public int Axe { get; set; }
        public int Hammer { get; set; }
        public int Crossbow { get; set; }
        public int Gun { get; set; }
        public int Shield { get; set; }
        public int Dodge { get; set; }
        public int Armor { get; set; }

        protected CharacterRow(Character character)
        {
            Character = character;

            Id = character.ID;

            Fitness = (int)(character.AttributeLevel(CharacterAttributeType.Fitness) * 100.0);
            Nimbleness = (int)(character.AttributeLevel(CharacterAttributeType.Nimbleness) * 100.0);
            Curiosity = (int)(character.AttributeLevel(CharacterAttributeType.Curiosity) * 100.0);
            Focus = (int)(character.AttributeLevel(CharacterAttributeType.Focus) * 100.0);
            Charm = (int)(character.AttributeLevel(CharacterAttributeType.Charm) * 100.0);

            Fighting = character.SkillLevel(CharacterSkillType.NaturalAttack);
            Brawling = character.SkillLevel(CharacterSkillType.Brawling);
            Sword = character.SkillLevel(CharacterSkillType.Sword);
            Axe = character.SkillLevel(CharacterSkillType.Axe);
            Hammer = character.SkillLevel(CharacterSkillType.Hammer);
            Crossbow = character.SkillLevel(CharacterSkillType.Crossbow);
            Gun = character.SkillLevel(CharacterSkillType.Gun);
            Shield = character.SkillLevel(CharacterSkillType.Shield);
            Dodge = character.SkillLevel(CharacterSkillType.Dodge);
            Armor = character.SkillLevel(CharacterSkillType.Armor);
        }

        public virtual void Save()
        {
            Character.SetAttributeLevel(CharacterAttributeType.Fitness, Fitness);
            Character.SetAttributeLevel(CharacterAttributeType.Nimbleness, Nimbleness);
            Character.SetAttributeLevel(CharacterAttributeType.Curiosity, Curiosity);
            Character.SetAttributeLevel(CharacterAttributeType.Focus, Focus);
            Character.SetAttributeLevel(CharacterAttributeType.Charm, Charm);
            
            Character.SetSkillLevel(CharacterSkillType.NaturalAttack, Fighting);
            Character.SetSkillLevel(CharacterSkillType.Brawling, Brawling);
            Character.SetSkillLevel(CharacterSkillType.Sword, Sword);
            Character.SetSkillLevel(CharacterSkillType.Axe, Axe);
            Character.SetSkillLevel(CharacterSkillType.Hammer, Hammer);
            Character.SetSkillLevel(CharacterSkillType.Crossbow, Crossbow);
            Character.SetSkillLevel(CharacterSkillType.Gun, Gun);
            Character.SetSkillLevel(CharacterSkillType.Shield, Shield);
            Character.SetSkillLevel(CharacterSkillType.Dodge, Dodge);
            Character.SetSkillLevel(CharacterSkillType.Armor, Armor);
        }

        public void SetMilitarySkills(int skillValue)
        {
            Fighting = skillValue;
            Brawling = skillValue;
            Sword = skillValue;
            Axe = skillValue;
            Hammer = skillValue;
            Crossbow = skillValue;
            Gun = skillValue;
            Shield = skillValue;
            Dodge = skillValue;
            Armor = skillValue;
        }

        public void SetAttributes(int attributeValue)
        {
            Fitness = attributeValue;
            Nimbleness = attributeValue;
            Curiosity = attributeValue;
            Focus = attributeValue;
            Charm = attributeValue;
        }
    }
}